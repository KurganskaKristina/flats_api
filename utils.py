def validate_input_data(input_data: dict):
    keys = ['house_price_index', 'quality', 'room_amount', 'total_square_meters', 'live_square_meters',
            'kitchen_square_meters', 'floor', 'floors_count', 'kitchen studio', 'multi-level', 'with_attic',
            'penthouse', 'without_furniture', 'rough_plaster', 'district_type_name_district',
            'district_type_name_suburb', 'district_type_name_village', 'city_name_Bila Tserkva', 'city_name_Bucha',
            'city_name_Irpin', 'city_name_Kyiv', 'city_name_Kyiv-Sviatoshynskyi', 'wall_type_108', 'wall_type_109',
            'wall_type_110', 'wall_type_111', 'wall_type_113', 'wall_type_115', 'wall_type_116', 'wall_type_1466',
            'wall_type_1467', 'wall_type_1616', 'wall_type_1620', 'wall_type_1621', 'wall_type_1625', 'heating_1648',
            'heating_1649', 'year_of_construction_435', 'year_of_construction_436', 'year_of_construction_437',
            'year_of_construction_441', 'year_of_construction_442', 'year_of_construction_1468',
            'year_of_construction_1469', 'year_of_construction_1470', 'year_of_construction_1471',
            'year_of_construction_1751', 'year_of_construction_1752', 'year_of_construction_1783',
            'year_of_construction_1784', 'year_of_construction_1789', 'year_of_construction_1791',
            'year_of_construction_1873']

    if type(input_data) != dict and len(input_data) != 53:
        return False

    for key in keys:
        if key not in input_data.keys():
            return False

    for value in input_data.values():
        if type(value) != list:
            return False

    return True


def get_price_range(class_number: int):
    if class_number == 1:
        return "до $10,000"
    if class_number == 2:
        return "від $10,000 до $20,000"
    if class_number == 3:
        return "від $20,000 до $25,000"
    if class_number == 4:
        return "від $25,000 до $30,000"
    if class_number == 5:
        return "від $30,000 до $35,000"
    if class_number == 6:
        return "від $35,000 до $40,000"
    if class_number == 7:
        return "від $40,000 до $45,000"
    if class_number == 8:
        return "від $45,000 до $50,000"
    if class_number == 9:
        return "від $50,000 до $55,000"
    if class_number == 10:
        return "від $55,000 до $60,000"
    if class_number == 11:
        return "від $60,000 до $65,000"
    if class_number == 12:
        return "від $70,000 до $80,000"
    if class_number == 13:
        return "від $80,000 до $100,000"
    if class_number == 14:
        return "від $100,000 до $150,000"
    if class_number == 15:
        return "від $150,000 до $200,000"
    if class_number == 16:
        return "від $200,000 до $250,000"
    if class_number == 17:
        return "від $250,000 до $300,000"
    if class_number == 18:
        return "від $300,000 до $500,000"
    if class_number == 19:
        return "від $500,000 до $1000,000"
    if class_number == 20:
        return "більше $1000,000"
