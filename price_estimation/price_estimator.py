import keras as k
import pandas as pd
from keras.layers import Dense
from sklearn.preprocessing import MinMaxScaler

model = k.Sequential([
    k.Input(shape=53),
    k.layers.Flatten(),
    k.layers.Dense(units=512, activation='relu'),
    k.layers.Dense(units=512, activation='relu'),
    k.layers.Dense(units=19, activation='softmax'),
])

model.compile(optimizer='Adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])

fit_results = model.load_weights(f'C:\/Users\/krist\/PycharmProjects\/flats_api\models\/flats_price_estimator_weights.h5')


class PriceEstimator:
    @staticmethod
    def __prepare_input_data(input_data):
        data = pd.read_csv(f'C:\/Users\/krist\/PycharmProjects\/flats_api\/price_estimation\/unnormalized_data.csv')
        df = pd.DataFrame.from_dict(input_data)
        data = data.append(df, ignore_index=True)
        data[
            ['house_price_index', 'room_amount', 'total_square_meters', 'live_square_meters', 'kitchen_square_meters',
             'quality', 'floor', 'floors_count']] = MinMaxScaler().fit_transform(
            data[['house_price_index', 'room_amount', 'total_square_meters', 'live_square_meters',
                  'kitchen_square_meters',
                  'quality', 'floor', 'floors_count']])
        df = data.iloc[-1].to_frame().transpose()
        df.reset_index(drop=True, inplace=True)

        return df

    @staticmethod
    def estimate_price(input_data):
        input_data = PriceEstimator.__prepare_input_data(input_data)
        y_pred = model.predict(input_data)
        max_y = 0
        estimated_class = 0

        for i in range(len(y_pred[0])):
            if y_pred[0][i] > max_y:
                max_y = y_pred[0][i]
                estimated_class = i + 1

        return estimated_class


if __name__ == '__main__':
    data = {'house_price_index': [114.9], 'quality': [10], 'room_amount': [10], 'total_square_meters': [43.2],
            'live_square_meters': [100],
            'kitchen_square_meters': [10], 'floor': [7], 'floors_count': [9], 'kitchen studio': [1], 'multi-level': [0],
            'with_attic': [0], 'penthouse': [1], 'without_furniture': [1], 'rough_plaster': [1],
            'district_type_name_district': [1], 'district_type_name_suburb': [0], 'district_type_name_village': [0],
            'city_name_Bila Tserkva': [0],
            'city_name_Bucha': [1],
            'city_name_Irpin': [0], 'city_name_Kyiv': [0], 'city_name_Kyiv-Sviatoshynskyi': [0],
            'wall_type_108': [1],
            'wall_type_109': [0],
            'wall_type_110': [0], 'wall_type_111': [0], 'wall_type_113': [0], 'wall_type_115': [0],
            'wall_type_116': [0],
            'wall_type_1466': [0], 'wall_type_1467': [0], 'wall_type_1616': [0], 'wall_type_1620': [0],
            'wall_type_1621': [0],
            'wall_type_1625': [0],
            'heating_1648': [0], 'heating_1649': [1],
            'year_of_construction_435': [0],
            'year_of_construction_436': [0], 'year_of_construction_437': [0], 'year_of_construction_441': [0],
            'year_of_construction_442': [0], 'year_of_construction_1468': [0], 'year_of_construction_1469': [0],
            'year_of_construction_1470': [0], 'year_of_construction_1471': [0], 'year_of_construction_1751': [0],
            'year_of_construction_1752': [0], 'year_of_construction_1783': [1], 'year_of_construction_1784': [0],
            'year_of_construction_1789': [0], 'year_of_construction_1791': [0], 'year_of_construction_1873': [0]}
    print(len(data))
    # input_data = PriceEstimator().prepare_input_data(data)
    # print(input_data)
    predicted_class = PriceEstimator().estimate_price(data)
    print(predicted_class)
