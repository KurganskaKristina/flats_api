import flask
from flask import Flask, request
from flask_cors import CORS, cross_origin
import json

from price_estimation.price_estimator import PriceEstimator
from utils import validate_input_data, get_price_range

app = Flask(__name__)

CORS(app)

app.config['CORS_HEADERS'] = 'Content-Type'


@app.route('/estimate', methods=['POST'])
def estimate_price():
    try:
        data = request.get_json()

        if validate_input_data(data):
            estimated_class = PriceEstimator.estimate_price(data)

            response = {
                'class': estimated_class,
                'estimated_price_range': get_price_range(estimated_class)
            }
            flask.Response(status=201)
            return json.dumps(response)
    except Exception as e:
        return flask.Response(status=400)


app.run(debug=True, port=5000)

if __name__ == '__main__':
    app.run(debug=True)
